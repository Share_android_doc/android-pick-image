package com.example.kimsoerhrd.demophotoobject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.example.kimsoerhrd.demophotoobject.Entity.Person;

public class Main2Activity extends AppCompatActivity {
        ImageView img;
    private static final String TAG = "Main2Activity";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        img = findViewById(R.id.imageView);
        Intent intent = getIntent();
        if (intent !=null){
            Person person = intent.getParcelableExtra("data");
            Log.e(TAG, "onCreate: "+ person.getFileName());
            Bitmap bitmap =BitmapFactory.decodeFile(person.getFileName());
            img.setImageBitmap(bitmap);
        }

    }
}

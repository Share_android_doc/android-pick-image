package com.example.kimsoerhrd.demophotoobject;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.kimsoerhrd.demophotoobject.Entity.Person;


public class MainActivity extends AppCompatActivity {

    private final int PICK_IMAGE=1;

    Button btnPick, btnSend;
    ImageView imageView;
    Bitmap bitmap;
    String fileName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnPick = findViewById(R.id.btnPhoto);
        btnSend = findViewById(R.id.btnSend);
        imageView = findViewById(R.id.Img);


        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                Bundle bundle = new Bundle();
                Person person = new Person();
                person.setFileName(fileName);
               // person.setBitmap(bitmap);
                bundle.putParcelable("data",person);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btnPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent,"SelectedPhoto"),PICK_IMAGE);

            }
        });

    }

    private static final String TAG = "MainActivity";
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && data.getData() !=null){

                //bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),uri);
                String[] columnIndex={MediaStore.Images.Media.DATA};
                Cursor cursor= getContentResolver().query(data.getData(),columnIndex,null,null,null);
                cursor.moveToFirst();
                int index=cursor.getColumnIndex(columnIndex[0]);
                fileName=cursor.getString(index);
                cursor.close();
            Log.e(TAG, "onActivityResult: "+fileName );
                bitmap=BitmapFactory.decodeFile(fileName);
                imageView.setImageBitmap(bitmap);

        }
    }
}
